import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';


const data = {
	title: "Zuitt Coding Bootcamp",
	content: "Opportunites for evryone, everywhere",
	destination: "/courses",
	label: "ENroll Now"

}


export default function Home() {
	return(
		<>
			<Banner data={data}/>
    		<Highlights/>
    		
    	</>
	)
}