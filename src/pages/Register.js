import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Register(){

	const {user, setUser} = useContext(UserContext);

	//State Hooks -> store values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobile, setMobile] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();

	// ? = if, : = else
	//Validation to enable register button when all fields are populated and both fields are match
	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	})

	//Function to simulate user registration

	function registerUser(e){
		//prevents page redirection via form submission
		e.preventDefault();

		setEmail(''); //to show blank 
		setPassword1('');
		setPassword2('');


		fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
				
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobile,
				password: password1,
				
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === false){
				Swal.fire({
					title: 'Successfully Registered!',
					icon: 'success',
					text: 'You may now able to enroll courses.'
				})

					// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component
				navigate("/courses");

			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}

		});
		
	}
	



	
	return (

		(user.id !== null) ?

			<Navigate to="/courses"/>

			:

		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group className="mb-3" controlid="firstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
	        </Form.Group>

	        <Form.Group className="mb-3" controlid="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
	        </Form.Group>
	        
	      <Form.Group className="mb-3" controlid="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlid="mobile">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control type="number" placeholder="Enter Mobile Number" value={mobile} onChange={e => setMobile(e.target.value)} required/>
	        </Form.Group>

	      <Form.Group className="mb-3" controlid="password1" >
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlid="password2" >
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
	      </Form.Group>
	     

  {/* {{Conditional Rendering -> IF active button is clickable -> IF Inactive button is not cliackable}}	*/}
	     {

	   
	      (isActive) ?	
	      <Button variant="primary" type="submit" controlId="submitBtn">
	        Register
	      </Button>
	      :
	      <Button variant="primary" type="submit" controlId="submitBtn" disabled>
	        Register
	      </Button>

			}

   		</Form>



		)
}